import { Component, OnInit } from '@angular/core';
import { faLinkedinIn, faGithub, faGitlab } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent /*implements OnInit*/ {
  iconLinkedin = faLinkedinIn;
  iconGitHub = faGithub;
  iconGitLab = faGitlab;

  constructor() {}

  /*ngOnInit(): void {}*/
}
